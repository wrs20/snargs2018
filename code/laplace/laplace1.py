"""

Code to solve

-\Delta U = c, for real valued c.

on [0,1] with U(0) = 0 and U(1) = 0. The approach will be
to recast the problem in the form

    Au=b

where u and b are approximations of real valued functions
defined on [0,1] via a vector of n values. The matrix A
when applied to these vectors is an approximation of the
Laplace operator.

"""

import numpy as np

c = 1.0
n = 400

# form a vector for the RHS
b = np.zeros(n)
b[:] = -1.0*c

# Construct the matrix A, the values are given from the
# finite difference approximation of the second derivative.

A = np.zeros((n, n))

# first row
A[0, 0] = -2.0
A[0, 1] = 1.0

# last row
A[-1, -1] = -2.0
A[-1, -2] =  1.0

# fill interior
for rowx in range(1, n-1):
    A[rowx, rowx-1:rowx+2:] = (1.0, -2.0, 1.0)

# solve Ax = b
x = np.linalg.solve(A,b)




# n points evenly distributed, includes endpoints by
# default
points = np.linspace(0.5/n,1-0.5/n,n)

# U(x) = -c/2 x*2 + c/2
correct = -0.5*c*points*points + 0.5 * c*points


import matplotlib.pyplot as plt
plt.plot(points, correct, 'r--',linewidth=3.0 )

# scale x by 1/(h^2)
x /= n*n


plt.plot(points, x)
plt.show()





















