"""
Use numpy to implement the Power method.

"""

import numpy as np

# Size of matrix
N = 100
# Desired tolerance
tol = 10.**-6

# Create a random matrix where the two largest magnitude
# eigenvalues are reasonably well separated
condA = 0.0
while condA < 1.0:
    A = np.random.uniform(low=-2.0, high=2.0, size=(N,N))
    w,v = np.linalg.eig(A)
    sw = np.sort(np.abs(w))
    condA = abs(sw[-1] - sw[-2])

# create an initial guess of [1/N, 1/N, ... , 1/N]
bk = np.ones(N)/N
# space to store the next iterate
bkp = np.zeros_like(bk)

# while residual is not small continue.
# This is potentially bad practice if convergence is not
# guaranteed.
residual = tol+1
while(abs(residual)>tol):

    # compute b^{K+1} = \frac{ A b^k }{ || A b^k || }
    bkp[:] = np.dot(A, bk)
    norm_bkp = np.linalg.norm(bkp)
    bkp[:] = bkp[:]/norm_bkp


    bk[:] = bkp[:]
    # use Rayleigh quotient to compute eigenvalue
    eigenvalue = np.dot(bk, np.dot(A, bk))

    # compute the error
    residual = np.linalg.norm(np.dot(A, bk) - eigenvalue*bk)

    print("{: .4e}".format(residual))


print("-"*40)
print("Ax-\lambda x:\t\t{: .4e}".format(np.linalg.norm(np.dot(A, bk) - \
    eigenvalue*bk)))
print("Eigenvalue error:\t{: .4e}".format(abs(w[0] - eigenvalue)))
err_vec = min(np.linalg.norm(v[:,0] - bk), np.linalg.norm(v[:,0] + bk))
print("Eigenvector error:\t{: .4e}".format(err_vec))


