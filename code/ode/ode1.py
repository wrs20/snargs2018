"""
The motion of a pendulum is described by

\frac{ \partial^2 y } { \partial^2 t } + (g/l) sin(y) = 0

where we shall take g/l=1. Rewrite in the form

d f(t,x) / dt = F(t, x)

where x = (x_0, x_1) and
F_0 = x_1
F_1 = -sin(x_0)

https://docs.scipy.org/doc/scipy/reference/integrate.html
"""


from math import sin

# import the RK45 implementation from scipy
from scipy.integrate import RK45

# define the function F such that
# d f(x,t) / dt = F(t,x)
def F(t, x):
    y = (
            x[1],
            -1. * sin(x[0])
            #-1.*x[0]
        )
    return y

# start time
t0 = 0
# maximum time
tmax = 10.0

# initial system state
x0 = (1., 0)

# create an instance of an integrator class
intg = RK45(F, t0, x0, tmax, max_step=0.01)

# make some python lists to store the results
time_set = []
theta_set = []
thetap_set = []

while intg.t < tmax:
    # integrate the system forward by one timestep
    intg.step()

    # store the system state for plotting
    theta_set.append(intg.y[0])
    thetap_set.append(intg.y[1])
    time_set.append(intg.t)

# plot results
import matplotlib.pyplot as plt
plt.plot(time_set, theta_set, 'b',linewidth=1.0 )
plt.plot(time_set, thetap_set, 'r--',linewidth=1.0 )
plt.show()


